﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Flashlight : MonoBehaviour
{

        public TextMeshProUGUI countText;
        private int count;
    // Start is called before the first frame update
    void Start()
    {
        count = 0;



    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
            this.GetComponent<Light>().enabled = true;
        if (Input.GetKeyUp(KeyCode.LeftShift))
            this.GetComponent<Light>().enabled = false;
        if (Input.GetKeyDown(KeyCode.LeftShift))
            this.GetComponent<CapsuleCollider>().enabled = true;
        if (Input.GetKeyUp(KeyCode.LeftShift))
            this.GetComponent<CapsuleCollider>().enabled = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obstacle"))
        {
            other.gameObject.SetActive(false);
        }
        // ..and if the GameObject you intersect has the tag 'Pick Up' assigned to it..
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);

            // Add one to the score variable 'count'
            count = count + 1;


        }
    }
}
